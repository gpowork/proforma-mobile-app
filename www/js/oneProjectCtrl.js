proformaMaster
.controller('ProjectCtrl', function($scope, $cordovaBarcodeScanner, $stateParams, $http, localStorageService, $ionicModal, sharedNote) {
  $scope.products = [];
  $scope.note = sharedNote.getNote();
  $scope.search = "";
  var uri = localStorageService.get('uri');
  $scope.callbackMethod = function(query) {
    return $http.jsonp(uri + '/product/search?q='+query+'&page=0&pageSize=20&callback=JSON_CALLBACK');
  }
  $ionicModal.fromTemplateUrl('productPage.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  var productMap = {};
  if ($stateParams.listId) {
    var url = uri+ '/notes/load/' + $stateParams.listId + '?callback=JSON_CALLBACK';
    $http.jsonp(url)
      .success(function(data){
        $scope.note = data.note;
        if (data && data.note && data.products) {
          var list = data.products;
          for (var i = 0; i < list.length; i++) {
            list[i].resizedPath = uri + list[i].resizedPath;
            list[i]['tempColors'] = '';
            productMap[list[i].id] = list[i];
          }
          var note = data.note;
          if (note.selectedProducts) {
            var arrPr = note.selectedProducts.split(";");
            if (arrPr && arrPr.length) {
              for (var i = 0; i < arrPr.length; i++){
                var row = arrPr[i];
                var arr = row.split(":");
                var pr = productMap[arr[0]];
                if (pr) {
                  pr['tempColorArray'] = pr['tempColorArray'] || [];
                  if (arr[1]) {
                    pr.tempColors = arr[1];
                    pr['tempColorArray'] = pr.colors.replace(/\s*/gi, '').split(",");
                  }
                }
              }
            }
          }
          $scope.products = list;
        }
      })
  }
  var addProductToList = function(productId, color) {
    if (productId != null) {
      var pr = productMap[productId];
      if (pr) {
        var colorArray = [];
        if (pr['tempColors']) {
          colorArray = pr.tempColors.split(",") || [];
        } else {
          pr['tempColors'] = '';
        }
        pr['tempColorArray'] = colorArray;
        if (color) {
          if (colorArray.indexOf(color) < 0) {
            colorArray.push(color);
            pr.tempColors = colorArray.join(",");            
          }
        }
        sharedNote.updateNote(productMap);
      } else {
        $http({url: localStorageService.get('uri') + '/product/ids/?ids='+productId, dataType:'json'})
          .then(function(res){
              if (res && res.data) {
                var product = res.data[0];
                if (product) {
                  productMap[productId] = product;
                  $scope.products.push(product);
                  addProductToList(productId, color);
                }
              }
            });
      }
    }
  }
  $scope.openScanner = function(){
    $cordovaBarcodeScanner
      .scan()
      .then(function(barcodeData) {
        var text = barcodeData.text;
        if (text) {
          var arr = text.split(":");
          if (arr && arr.length)
            addProductToList(arr[0], arr[1]);
        }
      }, function(error) {
        // An error occurred
      });
  }
  $scope.openProductPage = function(pr){
    if (pr != null) {
      var colorsMap = {};
      for (var i in pr.colorArray){
        var c = pr.colorArray[i].replace(/\s*/gi,'');
        colorsMap[c] = {
          color : c,
          selected : false
        };
        if (pr['tempColorArray']) {
          colorsMap[c].selected = (pr['tempColorArray'].indexOf(c) >= 0);
        }
      }
      $scope.colorsMap = colorsMap;
      $scope.pr = pr;
      $scope.modal.show();
    }
  }
  $scope.hideProductPage = function(){
    $scope.modal.hide();
  }
  $scope.saveSelections = function() {
    if ($scope.colorsMap) {
      var selected = [];
      for (var i in $scope.colorsMap) {
        var c = $scope.colorsMap[i];
        if (c.selected) {
          selected.push(i);
        }
      }
      $scope.pr.tempColors = selected.join(",");
      if (productMap[$scope.pr.id])
        productMap[$scope.pr.id].tempColors = $scope.pr.tempColors;
      sharedNote.updateNote(productMap);
    }
  }
})
.service('sharedNote', ['$http', 'localStorageService', function($http, localStorageService){
  var note = null;
  return {
    getNote : function() {
      return note;
    },
    setNote : function(_note) {
      note = _note;
    },
    updateNote : function(productMap){
      var arr = [];
      for (var i in productMap) {
        var pr = productMap[i];
        if (pr) {
          arr.push(pr.id + ':' + pr.tempColors);
        }
      }
      $http({url: uri + '/notes/update?noteId=' + $stateParams.listId, method:'POST', data: {selected : arr.join(";")}})
        .then(function(res){
          console.log(res);
        })
    },
    getNotes : function($scope) {
      var urlGetLists = localStorageService.get('uri') + '/notes/list/?callback=JSON_CALLBACK';
      $http({method:'JSONP', url:urlGetLists})
        .then(function(res){
          if (res && res.data && res.data.notes) {
            var list = res.data.notes;
            CACHE['notes'] = {};
            if (list != null && list.length > 0) {
              for (var i = 0; i < list.length; i++) {
                var selectedProducts = list[i].selectedProducts;
                var obj = {
                  numberOfProducts: 0
                };
                if (selectedProducts) {
                  var numberOfProducts = selectedProducts.split(";");
                  if (numberOfProducts && numberOfProducts.length)
                    obj.numberOfProducts = numberOfProducts.length;
                }
                CACHE['notes'][list[i].id] = angular.extend(obj, list[i]);
              }
            }
            $scope.notes = CACHE['notes'];
          }
        }, function(res){
          alert("Connection error.");
        });
    }
  }
}]);