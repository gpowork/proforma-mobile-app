// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'proforma.master', 'ngCordova', 'LocalStorageModule'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})
.config(function($stateProvider, $urlRouterProvider, localStorageServiceProvider, $sceDelegateProvider) {
  localStorageServiceProvider
    .setPrefix('proformav20');
  $stateProvider
    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'MenuCtrl'
    })
    .state('app.list', {
      url: '/list',
      views: {
        'menuContent': {
          templateUrl: 'templates/partials/notes.html',
          controller: 'NotesCtrl'
        }
      }
    })
    .state('app.single', {
      url: '/list/:listId',
      views : {
          'menuContent' : {
            templateUrl : 'templates/partials/singleNote.html',
            controller : 'SingleNoteCtrl'
          }
      }
    })
    .state('app.products', {
      url: '/products',
      views : {
          'menuContent' : {
            templateUrl : 'templates/partials/products.html',
            controller : 'ProductCtrl'
          }
      }
    })
    .state('app.settings', {
      url: '/settings',
      views : {
          'menuContent' : {
            templateUrl : 'templates/partials/settings.html',
            controller : 'SettingsCtrl'
          }
      }
    })
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/list');
  $sceDelegateProvider.resourceUrlWhitelist([
    // Allow same origin resource loads.
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'http://localhost:9000/**',
    'http://120.24.168.27/**'
  ]);
});