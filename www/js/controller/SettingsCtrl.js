proformaMaster
.controller('SettingsCtrl', function($scope, settingsSer){
	$scope.server = {
		serverUri : settingsSer.basePath
	}
	$scope.update = function() {
		settingsSer.saveBasePath($scope.server.serverUri);
	}
});