proformaMaster
.controller('SingleNoteCtrl', function($scope, $stateParams, noteSer, settingsSer, $cordovaBarcodeScanner, syncSer, $ionicLoading) {
	$scope.basePath = settingsSer.basePath;
	$scope.textArr = [];
	$scope.$on('$ionicView.enter', function(e) {
		if ($stateParams.listId) {
			noteSer.getNoteAndProducts($stateParams.listId)
				.then(function(note){
					$scope.note = note;
				});
		}
	});
	$scope.openScanner = function(){
	    $cordovaBarcodeScanner
	      .scan()
	      .then(function(barcodeData) {
	        var text = barcodeData.text;
	        if (text) {
	        	noteSer.addProductToNote($scope.note, text, true)
	        		.then(function(note){
	        			$scope.note = note;
	        		}, function(err){
	        			if (err && err.length > 0) {
	        				alert(err);
	        			}
	        		});
	        } else {
	        	alert("Can not recogineze. Please change position, check if enough light and try again.");	
	        }
	      }, 
	      function(error) {
	        alert("Can not recogineze.");
	      });
	}
	$scope.uploadNote = function(note) {
		$ionicLoading.show({
			template : 'Uploading to the server...'
		})
		syncSer.uploadNote(note, $scope)
			.then(function(){
				$ionicLoading.hide();
			})
	}
	$scope.showBig = false;
	$scope.openProductPage = function(pr) {
		noteSer.previewProduct(pr, $scope);
	}
	$scope.deleteProductFromNote = function(noteId, productId) {
		noteSer.getNoteById(noteId)
			.then(function(note){
				if (note && note.selectedProducts) {
					var arr = note.selectedProducts.split(";");
					var arrToSave = [];
					if (arr && arr.length > 0) {
						for (var i = 0; i < arr.length; i++) {
							var sub = arr[i].split(":");
							if (sub && sub.length > 0) {
								if (sub[0] != (productId + '')) {
									arrToSave.push(arr[i]);
								}
							}
						}
					}
					note.selectedProducts = arrToSave.join(";");
					noteSer.saveNote(note)
						.then(function(){
							$scope.note = note;
							for (var i = 0; i < note.products.length; i++) {
								if (note.products[i].id == productId) {
									note.products.splice(i, 1);
									break;
								}
							}
						})

				}
			})
	}
});