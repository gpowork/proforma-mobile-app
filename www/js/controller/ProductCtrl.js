proformaMaster
.controller('ProductCtrl', function($scope, $ionicModal, $timeout, loginSer, localStorageService, productSer, settingsSer, $ionicScrollDelegate, $cordovaBarcodeScanner) {
	$scope.basePath = settingsSer.basePath;
	$scope.products = [];
	var page = 0;
	var numberOnPage = 5;
	var stillHas = true;
	$scope.search = {
		text : ""
	};
	$scope.refreshScrollContent = function(){$ionicScrollDelegate.resize(); }
	$scope.getProducts = function(_refreshPage){
		if (_refreshPage) {
			page = 0;
			$scope.products = [];
		}
		productSer.getByName(page, numberOnPage, $scope.search.text)
			.then(function(list){
				if (list != null && list.length > 0) {
					for (var i = 0; i < list.length; i++) {
						$scope.products.push(list[i]);
					}
					$scope.$broadcast('scroll.infiniteScrollComplete');
					$scope.$broadcast('scroll.resize');
					$scope.refreshScrollContent();
					page++;
				} else {
					console.log("we don't have products anymore.");
					stillHas = false;
				}
			})
	}
	$scope.moreDataCanBeLoaded = function(){
		return stillHas;
	}
	$scope.$on('$ionicView.enter', function(e) {
		$scope.getProducts();
	});  		
	$scope.openProductPage = function(id){
		productSer.previewById(id, $scope);
	}
	$scope.openScanner = function(){
		try{
			$cordovaBarcodeScanner
		      .scan()
		      .then(function(barcodeData) {
		        var text = barcodeData.text;
		        if (text) {
		        	var arr = text.split(":");
		        	if (arr && arr.length > 0) {
		        		$scope.openProductPage(arr[0]);
		        	} else {
		        		alert("Can not recognize. Please change position, check if enough light and try again.")
		        	}
		        } else {
		        	alert("Can not recognize. Please change position, check if enough light and try again.");	
		        }
		      }, 
		      function(error) {
		        alert(JSON.stringify(error))
		      });
		  } catch(e){alert(JSON.stringify(e))}
	}
});