proformaMaster
.controller('MenuCtrl', function($scope, $ionicModal, $timeout, loginSer, localStorageService, $http, syncSer, $ionicLoading, $state, $ionicPlatform,$rootScope) {
	$scope.updateProgress = 0;
	if (!loginSer.isAuthorized($scope)) {
		$scope.callAfterLogin = function(){
			$scope.updateProgressSubject = '';
			$ionicLoading.show({
				scope: $scope,
				template : 'Syncronizing... {{updateProgressSubject}} {{updateProgress}}%<ion-spinner></ion-spinner>'
			})
			syncSer.start($scope)
				.then(function(){
					$ionicLoading.hide();
					$state.go('app.products', {});
				});
		}
		loginSer.openLoginBox($scope);
	} else {
		syncSer.start($scope)
			.then(function(){
				$state.go('app.products', {});
			});
	}
	$scope.sync = function() {
		$scope.updateProgressSubject = '';
		$ionicLoading.show({
			scope: $scope,
			template : 'Synchronizing... {{updateProgress}}%<ion-spinner></ion-spinner>'
		})
		syncSer.start($scope, true)
			.then(function(){
				$ionicLoading.hide();
				$state.go('app.products', {});
			});
	}
	syncSer.checkUpdates($scope);
	$rootScope.$on('updateProgress', function(evt, percent){
		$scope.updateProgress = percent.toFixed(1);
	});
	$rootScope.$on('updateProgressSubject', function(evt, updateProgressSubject){
		$scope.updateProgressSubject = updateProgressSubject;
	});
});