proformaMaster
.controller('NotesCtrl', function($scope, $ionicModal, $timeout, loginSer, localStorageService, $http, noteSer, $state, productSer, dbSer, $ionicLoading) {
	$scope.$on('$ionicView.enter', function(e) {
		noteSer.getListOfNotes()
			.then(function(notes){
				$scope.notes = notes;
			});
  	});
	$ionicModal.fromTemplateUrl('templates/partials/addNote.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
		$scope.addNoteModal = modal;
	});

	$scope.addNote = function(){
		var note = {
			listName : '',
			shortDescription : ''
		}
		$scope.note = note;
		$scope.addNoteModal.show();
	}
	$scope.createNote = function(){
		var note = $scope.note;
		if (!note.listName) {
			alert("Please put Name.");
			return;
		}
		if (!note.shortDescription) {
			alert("Please put Description.");
			return;
		}
		$ionicLoading.show({
			template : 'Sending to the server...'
		})
		noteSer.createNewNote(note, $scope)
			.then(function(note){
				$ionicLoading.hide()
				$scope.notes.push(note);
				$state.go('app.single', {listId: note.id});
				$scope.addNoteModal.hide();
			})
	}
});