proformaMaster
.service('dbSer', ['localStorageService', '$q', 'settingsSer'
,function(localStorageService, $q, settingsSer){
	var db = null;
	try{
		db = window.openDatabase("my.db2", '1', 'my', 1024 * 1024 * 50); // browser
	} catch(e){alert(JSON.stringify(e));}
    var dbObjects = {
    	product : {
    		tableName : 'product',
    		fields : {
    			id : 'integer primary key', 
    			articul : 'text', 
    			secondArticul : 'text', 
    			price : 'real', 
    			currency : 'text', 
    			chengben : 'real', 
    			length : 'text', 
    			width : 'text', 
    			height : 'text', 
    			scaleUnit : 'text', 
    			colors : 'text', 
    			material : 'text', 
    			numberOfPlafond : 'integer', 
    			netWeight : 'real', 
    			weightUnit : 'text', 
    			productType : 'text', 
    			createdBy : 'integer', 
    			modifiedBy : 'integer', 
    			image : 'text', 
    			hasQualityImg : 'integer', 
    			createdOn : 'text', 
    			modifiedOn : 'text',
    			deleted : 'text'
    		},
    		
    	},
    	note : {
    		tableName : 'note',
    		fields : {
    			id : 'integer primary key',
				listName : 'text',
				selectedProducts : 'text',
				searchStr : 'text',
				createdBy :'integer',
				createdOn : 'text',
				shortDescription : 'text',
				modifyDate : 'text',
				isDeleted : 'text'
    		}
    	},
    	grossWeight : {
    		tableName : 'grossWeight',
    		fields : {
    			id : 'integer primary key',
    			grossWeight : 'text',
    			position : 'integer',
    			plafNumber : 'integer',
    			productId : 'integer'
    		}
    	},
    	netWeight : {
    		tableName : 'netWeight',
    		fields : {
    			id : 'integer primary key',
    			netWeight : 'text',
    			position : 'integer',
    			plafNumber : 'integer',
    			productId : 'integer'
    		}
    	}
    }
	
	var dbUtil = {
		getCreateTableSql : function(dbObject) {
			var arr = [];
			for (var i in dbObject.fields) {
				arr.push(i + ' ' + dbObject.fields[i]);
			}
			return arr.join(',');
		},
		getKeyVal : function(obj, dbObject) {
			var res = {
				keys : [],
				vals : [],
				q : []
			}
			for (var i in obj) {
				if (dbObject.fields.hasOwnProperty(i)) {
    				res.keys.push(i);
    				res.vals.push(obj[i]);
    				res.q.push('?');
				}
			}
			return res;
		},
		getKeyValForUpdate : function(obj, dbObject) {
			var res = {
				keys : [],
				vals : []
			}
			for (var i in obj) {
				if (dbObject.fields.hasOwnProperty(i)) {
    				res.keys.push(i + '=?');
    				res.vals.push(obj[i]);
				}
			}
			res.vals.push(obj.id);
			return res;
		},
		getObjectFromRaw : function(raw, dbObject) {
			if (raw != null && dbObject != null) {
				var res = {};
				for (var i in dbObject.fields) {
					res[i] = raw[i];
				}
				return res;
			}
			return null;
		},
		runSQL : function(sql, params) {
			var d = $q.defer();
			if (sql) {
				db.transaction(function(tx) {
					tx.executeSql(sql,(params || []), function(tx, res){
						var updateArr = [];
						if (res.rows && res.rows.length) {
							for (var j = 0; j < res.rows.length; j++) {
								updateArr.push(res.rows.item(j).id);
							}
						}
						d.resolve(updateArr);
					})
				}, function(e) {
				  console.log('Transaction error: ' + e.message);
				}, function(res) {
					d.resolve();
				});
			} else {
				d.reject("Emty sql.");
			}
			return d.promise;
		},
		merge : function(list, dbObject, modifyColumn) {
			var d = $q.defer();
			if (list != null && list.length > 0) {
				var arrId = [];
				var toDelete = [];
				for (var i = 0; i < list.length; i++) {
					var notToDelete = true;
					if (list[i].hasOwnProperty('deleted') || list[i].hasOwnProperty('isDeleted')) {
						var prop = 'deleted';
						if (list[i].hasOwnProperty('isDeleted')) {
							prop = 'isDeleted';
						}
						if (list[i][prop] === "true" || list[i][prop] === true) {
							toDelete.push(list[i].id);
							notToDelete = false;
						}
					}
					if (notToDelete) {
						arrId.push(list[i].id);
					}
				}
				db.transaction(function(tx) {
					tx.executeSql('SELECT id FROM '+dbObject.tableName+' WHERE id in ('+arrId.join(',')+')',[], function(tx, res){
						var updateArr = [];
						var modifiedTracking = {};
						if (res.rows && res.rows.length) {
							for (var j = 0; j < res.rows.length; j++) {
								if (modifyColumn)
									modifiedTracking[res.rows.item(j).id] = res.rows.item(j)[modifyColumn] * 1;
								updateArr.push(res.rows.item(j).id);
							}
						}
						for (var i = 0; i < list.length; i++) {
							var keyVals = null
							if (updateArr.indexOf(list[i].id) > -1) {
								if (!modifyColumn || (modifyColumn && modifiedTracking[list[i].id] && modifiedTracking[list[i].id] > list[i][modifyColumn])) {
									keyVals = dbUtil.getKeyValForUpdate(list[i], dbObject);
									tx.executeSql('UPDATE ' + dbObject.tableName + ' SET '+keyVals.keys.join(', ')+' WHERE id=?', keyVals.vals);
								}
							} else {
								keyVals = dbUtil.getKeyVal(list[i], dbObject);
								tx.executeSql('INSERT OR REPLACE INTO ' + dbObject.tableName + ' (' + keyVals.keys.join(',') + ') VALUES('+keyVals.q.join(',')+')', keyVals.vals);
							}
						}
						if (toDelete.length > 0) {
							tx.executeSql('DELETE FROM ' + dbObject.tableName + ' WHERE id in ('+toDelete.join(",")+')');
						}
					});
				}, function(e) {
				  console.log('Transaction error: ' + e.message);
				}, function(res) {
					d.resolve();
				});
			} else {
				d.resolve();
			}
			return d.promise;
		},
		select : function(where, dbObject, page, numberOnPage, conditionsArr, orderBy) {
			var d = $q.defer();
			var offset = '';
			if (numberOnPage) {
				if (!page)
					page = 0;
				offset = ' LIMIT ' + numberOnPage + ' OFFSET ' + (page * numberOnPage);
			}
			var _orderBy = '';
			if (orderBy)
				_orderBy = ' ' + orderBy + ' ';
			db.transaction(function(tx) {
					var sql = 'SELECT * FROM ' + dbObject.tableName + ' ' + where  + _orderBy + offset;
					tx.executeSql(sql, conditionsArr, function(tx, res){
						var arr = null;
						if (res && res.rows.length > 0) {
							arr = [];
							for (var i = 0; i < res.rows.length; i++) {
								arr.push(dbUtil.getObjectFromRaw(res.rows.item(i), dbObject));
							}
						} else {
							console.log("empty");
						}
						if (numberOnPage == 1 && page == 0 && arr) {
							d.resolve(arr[0]);
						}
						d.resolve(arr);
					}, function(err){
						console.log("Error: " + sql);
					})
				}, function(e) {
				  console.log('Transaction error: ' + e.message);
				}, function(res) {
					d.resolve();
					console.log('db transaction is done.');
				});
			return d.promise;
		}
	};

	db.transaction(function(tx) {
	  tx.executeSql('CREATE TABLE IF NOT EXISTS '+dbObjects.product.tableName+' (' + dbUtil.getCreateTableSql(dbObjects.product)  + ')');
	  tx.executeSql('CREATE TABLE IF NOT EXISTS '+dbObjects.note.tableName+' (' + dbUtil.getCreateTableSql(dbObjects.note)  + ')');
	  tx.executeSql('CREATE TABLE IF NOT EXISTS '+dbObjects.grossWeight.tableName+' (' + dbUtil.getCreateTableSql(dbObjects.grossWeight)  + ')');
	  tx.executeSql('CREATE TABLE IF NOT EXISTS '+dbObjects.netWeight.tableName+' (' + dbUtil.getCreateTableSql(dbObjects.netWeight)  + ')');
	}, function(e) {
	  console.log('Transaction error: ' + e.message);
	}, function(res) {
		// console.log(res);
	});
	
	return {
		syncLocalWithServer : function(map, key, dbObject) {
			var d = $q.defer();
			if (map) {
				var tasks = [];
				for (var keyval in map) {
					if (map[keyval] && map[keyval].length > 0) {
						// get array of ids which come from the server:
						var arrIds = [];
						map[keyval].forEach(function(w){
							arrIds.push(w.id);
						});
						// Delete all which are in local DB and not in the list from server
						tasks.push(dbUtil.runSQL('DELETE FROM ' + dbObject.tableName + ' WHERE ' + key + '=? AND id NOT IN ('+arrIds.join(',')+')',[keyval]));
						// Merge all which compe from the server
						tasks.push(dbUtil.merge(map[keyval], dbObject));
					} else {
						// delete all childs
						tasks.push(dbUtil.runSQL('DELETE FROM ' + dbObject.tableName + ' WHERE ' + key + '=?',[keyval]));
					}
				}
				return $q.all(tasks);
			}
			d.resolve();
			return d.promise;
		},
		saveListOfProducts : function(list) {
			var d = $q.defer();
			var self = this;
			if (list != null && list.length > 0) {
				dbUtil.merge(list, dbObjects.product)
					.then(function(){
						var grossWeightArr = {};
						var netWeightArr = {};
						list.forEach(function(pr){
							grossWeightArr[pr.id] = [];
							if (pr.grossWeights && pr.grossWeights.length) {
								pr.grossWeights.forEach(function(gw){
									grossWeightArr[pr.id].push(angular.extend({productId : pr.id}, gw));
								});
							}
							if (pr.netWeights && pr.netWeights.length) {
								netWeightArr[pr.id] = [];
								pr.netWeights.forEach(function(nw){
									netWeightArr[pr.id].push(angular.extend({productId : pr.id}, nw));
								});
							}
						});
						$q.all([
							self.syncLocalWithServer(grossWeightArr, 'productId', dbObjects.grossWeight),
							self.syncLocalWithServer(netWeightArr, 'productId', dbObjects.netWeight)
						]).then(function(){
							d.resolve();
						})
					})
			} else {
				d.resolve();
			}
			return d.promise;
		},
		saveListOfNotes : function(list) {
			return dbUtil.merge(list, dbObjects.note);	
		},
		getListOfProducts : function(page, numberOnPage, orderBy, where, params) {
			var _where = '';
			if (where && where.length)
				_where = where;
			return dbUtil.select('' + _where, dbObjects.product, page, numberOnPage, params, orderBy);
		},
		getListOfNotes : function(page, numberOnPage) {
			return dbUtil.select('', dbObjects.note, page, numberOnPage);
		},
		getNoteById : function(noteId) {
			return dbUtil.select(' WHERE id=?', dbObjects.note, 0, 1, [noteId]);	
		},
		getNoteByName : function(name) {
			return dbUtil.select(' WHERE listName=?', dbObjects.note, 0, 1, [name]);	
		},
		getProductById : function(id) {
			return dbUtil.select(' WHERE id=' + id, dbObjects.product, 0, 1,[]);	
		},
		getProductByIds : function(ids, page, numberOnPage) {
			return dbUtil.select(' WHERE id in ('+ids+')', dbObjects.product, page, numberOnPage, null);	
		},
		getGrossWeight : function(productId) {
			return dbUtil.select(' WHERE productId = ? ORDER BY position', dbObjects.grossWeight, 0, 100, [productId]);
		},
		getNetWeight : function(productId) {
			return dbUtil.select(' WHERE productId = ? ORDER BY position', dbObjects.netWeight, 0, 100, [productId]);
		},
		updateProductsInNote : function(note) {
			return dbUtil.runSQL('UPDATE ' + dbObjects.note.tableName + ' SET selectedProducts=?, modifyDate=? WHERE id=?',[note.selectedProducts, (new Date()).getTime(), note.id]);
		},
		makeDumpProducts : function() {
			dbUtil.select(' ORDER BY id', dbObjects.product, 0, 5000)
				.then(function(list){
					if (list)
						for (var i = 0; i < list.length; i++)
							console.log(JSON.stringify(list[i]));
				})
		}
	}
}]);