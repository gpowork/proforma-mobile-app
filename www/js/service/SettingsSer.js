proformaMaster
.service('settingsSer', ['localStorageService', '$http', '$q', '$ionicModal',
function(localStorageService, $http, $q, $ionicModal){
	var _basePath = localStorageService.get('basePath');
	if (!_basePath)
		 _basePath = 'http://localhost:9000';
		// _basePath = 'http://192.168.0.103:9000';
		// _basePath = 'http://120.24.168.27';
	return {
		basePath : _basePath,
		saveBasePath : function(bP) {
			_basePath = bP;
			this.basePath = bP;
			localStorageService.set('basePath', bP);
		}
	}
}]);