proformaMaster
.service('loginSer', ['localStorageService', '$http', '$q', '$ionicModal', 'settingsSer',
	function(localStorageService, $http, $q, $ionicModal, settingsSer){
	var session = {
		isAuthorized : false,
		user : {
			email : null,
			password : null
		},
		login : function() {
			if (!this.user.email) {
				alert("Please put your email.");
				return;
			}
			if (!this.user.password) {
				alert("Please put your password.");
				return;
			}
			$http.jsonp(settingsSer.basePath + '/ajaxSignin?callback=JSON_CALLBACK&email=' + session.user.email + '&password=' + session.user.password)
				.then(function(res){
					if (res && res.data == "true") {
						localStorageService.set('user', session.user);
						session.isAuthorized = true;
						session.loginBox.hide();
						if (typeof session.callAfterLogin != "undefined")
							session.callAfterLogin();
					} else {
						alert("Email or password is not correct. Please check and try again.");
					}
				},
				function(err){
					var message = "Can not connect to the server. Please check your internet connection and try again. " + settingsSer.basePath;
					alert(message);
				})
		},
		logout : function() {
			localStorageService.set('user', null);
			localStorageService.set('lastSyncTime', null);
			window.location.reload();
		}
	};
	var user = localStorageService.get('user');
	if (user) {
		session.isAuthorized = true;
		session.user = user;
	}
	var getLoginBox = function(scope) {
		var d = $q.defer();
		scope.session = session;
		if (!scope.loginBox) {
			$ionicModal.fromTemplateUrl('templates/partials/login.html', {
				scope: scope,
				animation: 'slide-in-up'
			}).then(function(modal) {
				session.loginBox = modal;
				if (typeof scope.callAfterLogin != "undefined")
					session['callAfterLogin'] = scope.callAfterLogin;
				d.resolve(session.loginBox);
			});
		} else {
			d.resolve(session.loginBox);
		}
		return d.promise;
	};
	return {
		getUser : function(scope){
			if (!session.user) {
				session.user = localStorageService.get("user");
				if (!session.user) {
					this.openLoginBox(scope);
					return;
				}
			}
			return session.user;
		},
		isAuthorized : function(scope) {
			scope.session = session;
			return session.isAuthorized;
		},
		openLoginBox : function(scope) {
			getLoginBox(scope)
				.then(function(modal){
					modal.show();
				});
		},
		login : function(email, password) {
			var d = $q.defer();
			var url = settingsSer.basePath + '/ajaxSignin?callback=JSON_CALLBACK&email=' + email
		        + '&password=' + password;
		    $http.jsonp(url)
		      .success(function(data){
		        if (data) {
		            localStorageService.set('user', session.user);
					session.isAuthorized = true;	
		          	d.resolve(data);
		        } else {
		          d.reject('Email or password is not right.');
		        }
		      });
		    return d.promise;
		},
	}
}])