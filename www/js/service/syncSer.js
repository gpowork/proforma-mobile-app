proformaMaster
.service('syncSer', ['localStorageService', '$http', '$q', 'settingsSer', 'loginSer', 'dbSer', '$state', '$ionicPlatform', '$rootScope'
,function(localStorageService, $http, $q, settingsSer, loginSer, dbSer, $state, $ionicPlatform, $rootScope){
	var lastSyncTime = localStorageService.get('lastSyncTime');
	var _download = function(url, returnProp) {
		var d = $q.defer();
		$http.jsonp(settingsSer.basePath + url+'&callback=JSON_CALLBACK')
			.then(function(res){
				if (res && res.data) {
					if (returnProp)
						d.resolve(res.data[returnProp]);
					else
						d.resolve(res.data);
				} else {
					d.resolve(null);
				}
			}, function(err){
				alert("Problems with connection to the server. Please check your internet connection and try again.");
			})
		return d.promise;
	}
	var totalRows = 0;
	var totalDownloaded = 0;
	var downloadProducts = function(lastPage) {
		if (!lastPage)
			lastPage = 0;
		if (lastPage == 0) {
			totalRows = 0;
			totalDownloaded = 0;
		}
		var d = $q.defer();
		$rootScope.$broadcast('updateProgressSubject', 'Products');
		_download('/product/loadupdated?page=' + lastPage + '&perPage=40&lastUpdateTime=' + lastSyncTime, null)
			.then(function(map){
				if (map.totalRows) {
					totalRows = map.totalRows;
				}
				var list = map['productList'];
				if (list && list.length > 0) {
					dbSer.saveListOfProducts(list)
						.then(function(){
							totalDownloaded += list.length;
							$rootScope.$broadcast('updateProgress', totalDownloaded / totalRows * 100);
							downloadProducts(++lastPage)
								.then(function(){
									d.resolve();
								})
						});
				} else {
					d.resolve(0);
				}
			})
		return d.promise;
	}
	var totalNotesDownloaded = 0;
	var totatlNotes = 0;
	var downloadNotes = function(lastPage) {
		if (!lastPage)
			lastPage = 0;
		if (lastPage == 0) {
			totalNotesDownloaded = 0;
			totatlNotes = 0;
		}
		var d = $q.defer();
		$rootScope.$broadcast('updateProgressSubject', 'Notes');
		_download('/notes/list/?showDeleted=true&page=' + lastPage, 'notes')
			.then(function(list){
				if (list && list.length > 0) {
					dbSer.saveListOfNotes(list)
						.then(function(){
							downloadNotes(++lastPage)
								.then(function(){
									totalNotesDownloaded += list;
									$rootScope.$broadcast('updateProgress', totalNotesDownloaded / totatlNotes * 100);
									d.resolve();
								});
						});
				} else {
					d.resolve();
				}
			})
		return d.promise;
	}
	return {
		start : function(scope, force) {
			var d = $q.defer();
			if (force || (!lastSyncTime || ((new Date()).getTime() - lastSyncTime) > 600000)) {
				var user = loginSer.getUser(scope);
				loginSer.login(user.email, user.password)
					.then(function(){
						downloadProducts(0)
							.then(function(){
								downloadNotes(0)
									.then(function(){
										localStorageService.set('lastSyncTime', (new Date()).getTime());
										$state.go('app.list', {});
										d.resolve();
									})
							})
					})
			}
			return d.promise;
		},
		uploadNote : function(note, scope) {
			var d = $q.defer();
			var user = loginSer.getUser(scope);
			loginSer.login(user.email, user.password)
				.then(function(res){
					$http.post(settingsSer.basePath + '/notes/update?noteId=' + note.id, {selected: note.selectedProducts})
						.then(function(res){
							d.resolve(res);
						});
				})
			return d.promise;	
		},
		createNote : function(note, scope) {
			var d = $q.defer();
			var user = loginSer.getUser(scope);
			loginSer.login(user.email, user.password)
				.then(function(res){
					$http.post(settingsSer.basePath + '/notes/save?returnObject=true', note)
						.then(function(res){
							if (res.data) {
								var serversNote = res.data;
								dbSer.saveListOfNotes([serversNote])
									.then(function(){
										d.resolve(serversNote);
									})
							}
						});
				})
			return d.promise;
		},
		checkUpdates : function(scope) {
			var checkVersion = function(callback) {
				$http.get(settingsSer.basePath + '/storage/get/apk')
				.then(function(res){
					var data = res.data;
					callback(data.version, data.apkFile);
				});
			}
			var methodConfirmDownload = function(callback) {
				callback(true);
			}
			var methodRunInstaller = function(callback) {
				if (confirm("There is new version of App. Do you want install it?")) {
					callback(true);
				}
			}
			var methodSuccess = function(){
				alert("App installed successful! Please restart app to apply all changes.");
			}
			var methodError = function(){
				alert("Error occured during installation. Please contact IT support.");
			}
			if (window.apkUpdator) {
				$ionicPlatform.ready(function() {
					apkUpdator.init(
						checkVersion, methodConfirmDownload, function(){}, methodRunInstaller, methodSuccess, methodError
					);
				}, false)
			}
		}
	}
}]);