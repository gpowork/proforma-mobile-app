proformaMaster
.service('productSer', ['$q', 'settingsSer', 'dbSer', '$ionicModal',function($q, settingsSer, dbSer, $ionicModal){
	var cache = {};
	var modal = null;
	var getModal = function(scope){
		scope.basePath = settingsSer.basePath;
		var d = $q.defer();
		$ionicModal.fromTemplateUrl('templates/partials/singleProduct.html', {
			scope: scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			scope.previewProduct = modal;
			d.resolve(modal);
		});
		return d.promise;
	}
	return {
		getByIds : function(ids) {
			var d = $q.defer();
			var self = this;
			dbSer.getProductByIds(ids, 0, 0)
					.then(function(list){
						if (list != null && list.length > 0) {
							for (var i = 0; i < list.length; i++) {
								var pr = list[i];
								if (pr) {
									cache[pr.id] = pr;
									self.replaceResized(pr);
								}
							}
						}
						d.resolve(list);	
					})
			return d.promise;
		}, 
		getById : function(id) {
			var d = $q.defer();
			var self = this;
			if (cache[id]) {
				d.resolve(cache[id]);
			} else {
				$q.all([
					dbSer.getProductById(id),
					dbSer.getGrossWeight(id),
					dbSer.getNetWeight(id)
				]).then(function(res){
					var pr = res[0], grossList = res[1], netList = res[2];
					if (pr) {
						cache[id] = angular.extend({grossWeights:grossList, netWeights:netList}, pr);
						self.replaceResized(cache[id]);
						d.resolve(cache[pr.id]);	
					} else {
						d.reject("no_product_with_this_id");
					}
				})
			}
			return d.promise;
		},
		previewById : function(id, scope) {
			var self = this;
			if (cache[id])
				cache[id] = null;
			this.getById(id)
				.then(function(pr){
					scope.pr = pr;
					self.replaceResized(pr);
					scope.showColorSelect = false;
					if (scope.note) {
						scope.showColorSelect = true;
						scope.colorsMap = {};
						if (pr.colors) {
							var arr = pr.colors.split(",");
							if (arr != null && arr.length) {
								for (var i = 0; i < arr.length; i++) {
									if (arr[i]) {
										var col = arr[i].trim().replace(/\s*/g, '');
										scope.colorsMap[col] = {name : col, val : false};
									}
								}
							}
						}
						if (scope.selectedProduct) {
							var arr = scope.selectedProduct.tempColors.split(",");
							if (arr && arr.length) {
								for (var i = 0; i < arr.length; i++) {
									if (arr[i]) {
										var c = arr[i].trim().replace(/\s*/g, '');
										if (scope.colorsMap[c]) {
											scope.colorsMap[c].val = true;
										}
									}
								}
							}
						}
						scope.updateColorsInNote = function() {
							var arr = [];
							for (var c in scope.colorsMap) {
								if (scope.colorsMap[c].val) {
									arr.push(c);
								}
							}
							scope.selectedProduct.tempColors = arr.join(",");
							var prArr = [];
							for (var i = 0; i < scope.note.products.length; i++) {
								var pr = scope.note.products[i];
								if (pr.id == scope.selectedProduct.id) {
									prArr.push(pr.id + ":" + scope.selectedProduct.tempColors);
								} else {
									prArr.push(pr.id + ":" + pr.tempColors);
								}
							}
							scope.note.selectedProducts = prArr.join(";");
							return dbSer.updateProductsInNote(scope.note);
						}
					}
					getModal(scope)
						.then(function(modal){
							modal.show();
						})
				})
		},
		replaceResized : function(el) {
			if(el.image) {
				var arr = el.image.split(".");
				var last = arr.pop();
				el['resizedPath'] = arr.join('.') + 'resized.' + last;
			}
		},
		getByName : function(number, page, searchStr) {
			var d = $q.defer();
			var self = this;
			var where = '';
			var params = [];
			var statements = [];
			if (searchStr && searchStr.length) {
				var tokens = searchStr.split(",");
				if (tokens) {
					tokens.forEach(function(t){
						if (t && t.trim()){
							statements.push(' articul LIKE ? ');
							params.push(t.trim());
						}
					});
				}
				where = ' WHERE ' + statements.join(' OR ');
			}
			dbSer.getListOfProducts(number, page, 'ORDER BY id DESC', where, params)
				.then(function(list){
					if (list != null && list.length > 0) {
						for (var i = 0; i < list.length; i++) {
							self.replaceResized(list[i]);
						}
					}
					d.resolve(list);
				})
			return d.promise;
		}
	}
}]);