proformaMaster
.service('noteSer', ['$q', 'settingsSer', 'dbSer', 'productSer', 'syncSer', function($q, settingsSer, dbSer, productSer, syncSer){
	var cache = {};
	var saveProducts = function(prArr, note, forceColor) {
		var thrArr = [];
		var d = $q.defer();
		if (prArr && prArr.length) {
			prArr.forEach(function(pr){
				var prodId = pr.id;
				var color = pr.colors;
				if (forceColor) {
					color = forceColor;
				}
				var isNewProduct = false;
				if (!note['colorsPerProduct'][prodId]) {
					note['colorsPerProduct'][prodId] = "";
					isNewProduct = true;
				}
				var colorsArr = note['colorsPerProduct'][prodId].split(",");	
				if (color && colorsArr.indexOf(color) < 0)
					colorsArr.push(color);
				note['colorsPerProduct'][prodId] = colorsArr.join(",");	
				pr['tempColors'] = note.colorsPerProduct[pr.id];
				var selectedArr = [];
				if (!note.products)
					note.products = [];
				if (isNewProduct)
					note.products.push(pr);		
				for (var i = 0; i < note.products.length; i++) {
					var prOne = note.products[i];
					selectedArr.push(prOne.id + ":" + prOne.tempColors);
				}
				note.selectedProducts = selectedArr.join(";");
				thrArr.push(dbSer.updateProductsInNote(note));	
			})
			$q.all(thrArr)
				.then(function(){
					d.resolve(note);
				})
		} else {
			d.resolve(note);
		}
		return d.promise;
	}
	return {
		getNoteById : function(noteId) {
			var d = $q.defer();
			if (cache[noteId]) {
				d.resolve(cache[noteId]);
			} else {
				dbSer.getNoteById(noteId)
					.then(function(note){
						cache[noteId] = note;
						d.resolve(cache[noteId]);
					})
			}
			return d.promise;
		},
		getListOfNotes : function() {
			var d = $q.defer();
			dbSer.getListOfNotes()
				.then(function(notes){
					if (notes != null && notes.length > 0) {
						for (var i = 0; i < notes.length; i++) {
							var note = notes[i];
							cache[note.id] = note;
							if (note.selectedProducts) {
					            var arrPr = note.selectedProducts.split(";");
					            note['numberOfProducts'] = arrPr.length;
					          }
						}
					}
					d.resolve(notes);					
				})
			return d.promise;
		},
		getNoteAndProducts : function(noteId) {
			var d = $q.defer();
			this.getNoteById(noteId)
				.then(function(note){
					if (note) {
						if (!note.selectedProducts)
							note.selectedProducts = '';
						var arrPr = note.selectedProducts.split(";");
			            note['numberOfProducts'] = arrPr.length;
			            note['colorsPerProduct'] = {};
			            if (arrPr && arrPr.length) {
			            	var productsIdArr = [];
							for (var i = 0; i < arrPr.length; i++){
								var row = arrPr[i];
								var arr = row.split(";");
								if (arr && arr.length) {
									for (var j = 0; j < arr.length; j++) {
										var row = arr[j];
										var subarr = row.split(":");
										if (subarr && subarr.length) {
											productsIdArr.push(subarr[0]);
											note['colorsPerProduct'][subarr[0]] = subarr[1];
										}
									}
								}
							}
							productSer.getByIds(productsIdArr.join(","))
								.then(function(products){
									if (products != null && products.length > 0) {
										note['products'] = products;
										for (var j = 0; j < products.length; j++) {
											var pr = products[j];
											if (pr) {
											  pr['tempColors'] = note.colorsPerProduct[pr.id];
											}
										}
									}
								});
			            }
			            d.resolve(note);
					}
				});
			return d.promise;
		},
		createNewNote : function(note, scope){
			var d = $q.defer();
			var self = this;
			syncSer.createNote(note, scope)
				.then(function(nt){
					if (nt) {
						self.getNoteById(nt.id)
							.then(function(_note){
								d.resolve(_note);
							})
					}
				})
			return d.promise;
		},
		addProductToNote : function(note, text, allSerial) {
			var d = $q.defer();
			if (text != null) {
				var arr = text.split(":");
				if (arr && arr.length > 0) {
					var prodId = arr[0];
					var color = arr[1];
						productSer.getById(prodId)
							.then(function(pr) {
								if (pr) {
									if (allSerial) {
										// Save all products from this serial
										var serialArticul = pr.articul.split("/");
										if (serialArticul && serialArticul.length) {
											productSer.getByName(100, 0, serialArticul[0] + '%')
												.then(function(prList){
													saveProducts(prList, note)
														.then(function(){
															d.resolve(note);
														});
												});
										}
									} else {
										// Save just one selected product (old way)
										saveProducts([pr], note, color)
											.then(function(){
												d.resolve(note);
											});
									}
								} else {
									d.reject();
								}
							},
							function(err){
								d.reject("Did not found product in local DB. Synchronize it and try again.");
							});
				} else {
					d.reject("Wrong QR code.");
				}
			} else {
				d.reject("Wrong QR code.");
			}
			return d.promise;
		},
		previewProduct : function(pr, scope) {
			scope.selectedProduct = pr;
			productSer.previewById(pr.id, scope);
		},
		saveNote : function(note) {
			return dbSer.saveListOfNotes([note]);
		}
	}
}]);