proformaMaster
.service('loginService', ['localStorageService', '$http', '$q', function(localStorageService, $http, $q){
	var user = null;
	return {
		getUser : function(){
			if (!user) {
				user = JSON.parse(localStorageService.get("loginData"));
			}
			return user;
		},
		checkIfValid : function() {
			return (localStorageService.get('passedAuth') == true);
		},
		login : function(email, password) {
			var url = localStorageService.get('uri') + '/ajaxSignin?callback=JSON_CALLBACK&email=' + email
		        + '&password=' + password;
		    return $http.jsonp(url)
		      .success(function(data){
		        if (data) {
		          localStorageService.set('passedAuth', true);
		          user.email = email;
		          user.password = password;
		          localStorageService.set('loginData', JSON.stringify(user));
		          $q.resolve();
		        } else {
		          $q.reject('Email or password is not right.');
		        }
		      });
		},
		logout : function(){
			localStorageService.set('passedAuth', false);
			localStorageService.set('loginData', null);
		}
	}
}])